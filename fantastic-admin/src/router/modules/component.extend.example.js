import Layout from '@/layout'

export default {
    path: '/component_extend_example',
    component: Layout,
    redirect: '/component_extend_example/pageheader',
    name: 'componentExtendExample',
    meta: {
        title: '新增博客内容',
        icon: 'sidebar-component'
    },
    children: [
        {
            path: 'bgImg',
            name: 'BgImg',
            component: () => import(/* webpackChunkName: 'component_extend_example' */ '@/views/component_extend_example/bgImg'),
            meta: {
                title: '新增左侧背景图'
            }
        },
        {
            path: 'editor/:id',
            name: 'componentExtendExampleEditor',
            component: () => import(/* webpackChunkName: 'component_extend_example' */ '@/views/component_extend_example/editor'),
            meta: {
                title: '新增博客'
            }
        }

    ]
}
