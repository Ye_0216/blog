import Layout from '@/layout'

// 博客路由
export default {
    path: '/administer',
    component: Layout,
    redirect: '/administer/blog',
    name: 'multilevelMenuExample',
    meta: {
        title: '博客内容列表',
        icon: 'sidebar-menu'
    },
    children: [
        {
            path: 'blog',
            name: 'Blog',
            component: () => import(/* webpackChunkName: 'multilevel_menu_example' */ '@/views/multilevel_menu_example/blog'),
            meta: {
                title: '博客列表'
            }
        },
        {
            path: 'leaveMessage',
            name: 'LeaveMessage',
            component: () => import(/* webpackChunkName: 'multilevel_menu_example' */ '@/views/multilevel_menu_example/leaveMessage'),
            meta: {
                title: '留言列表'
            }
        },
        {
            path: 'background',
            name: 'Background',
            component: () => import(/* webpackChunkName: 'multilevel_menu_example' */ '@/views/multilevel_menu_example/background'),
            meta: {
                title: '左侧背景'
            }
        }
    ]
}
