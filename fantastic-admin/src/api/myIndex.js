import myAxios from './myAxios'
// 接口图片前缀
export const baseURL = 'http://110.40.167.170:8889'
// 图片上传action接口
export const action = 'http://110.40.167.170:8889/api/uploadAction'

// 获取所有博客列表
export let getBlogList = () => myAxios.get('/getArticle')
// 获取所有留言内容
export let getRepList = () => myAxios.get('/getRep')
// 获取所有背景图
export let getBgImgpList = () => myAxios.get('/getBgImg')
// 获取指定博客
export let getBlogById = id => myAxios.post('/getBlogById', {id})
// 添加新博客
export let addBlog = (title, mainContent, minorContent, img) => myAxios.post('/addBlog', {title, mainContent, minorContent, img})
// 删除博客
export let delBlog = id => myAxios.post('/delBlog', {id})
// 更新博客
export let updateBlog = (id, title, mainContent, updateTime, minorContent, img) => myAxios.post('/updateBlog', {id, title, mainContent, updateTime, minorContent, img})
// 删除指定留言内容
export let delComments = id => myAxios.post('/delComments', {id})

// 登录接口
export let login = (name, password) => myAxios.post('/login', {name, password})
// 更新用户信息接口
export let updateUserInfo = (userid, chinesename, englishname, userinfo, profile) => myAxios.post('/updateUserInfo', {userid, chinesename, englishname, userinfo, profile})
// 新增背景图
export let addBgImg = (title, imgPath) => myAxios.post('/addBgImg', {title, imgPath})
// 删除背景图
export let delBgImg = id =>  myAxios.post('/delBgImg', {id})

// 上传图片，并将其路径返回
export let upLoadImg = fd => myAxios.post('upload', fd)

// 留言审核
export let auditComment = id => myAxios.post('/auditComment', {id})

// 获取访问表数据
export let getAccess = () => myAxios.post('/access')

// 获取博客数和留言数
export let getBlogAndMessgeSize = () => myAxios.get('/getBlogAndMessgeSize')

