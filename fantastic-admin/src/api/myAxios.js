import axios from 'axios'
import { Message } from 'element-ui'
const myAxios = axios.create({

    baseURL: 'http://110.40.167.170:8889/api',
    timeout: 4000
})

myAxios.interceptors.request.use(config => {
    return config
})

myAxios.interceptors.response.use(
    response => {
        return response.data
    },
    error => {
        console.log(error)
        Message.error('连接超时!')
        return new Promise(()  => {})
    }
)

export default myAxios
