import myAxios from './myAxios'

// 接口前缀
export const BASEURL = 'http://110.40.167.170:8889'
// export const BASEURL = 'http://127.0.0.1:8888'
// 点赞flag值
const FLAGAGREE = 1
// 反对flag值
const FLAGOPPOSE = 0

// 获取博客主人的信息
export const getUserInfo = () => myAxios.get('/userInfo',)

// 获取左侧背景图
export const getBgImg = () => myAxios.get('/getBgImg')
//获取文章列表
export const getBlogList = () => myAxios.get('/getArticle')

// 获取分页博客
export const getPageBlog = page => myAxios.post('/pageBlog', { page })
// 获取分页留言
export const getPageRep = page => myAxios.post('/pageRep', { page })

//获取留言板内容
export const getMessageList = () => myAxios.get('/getRep')

//保存用户的留言信息
export const saveMessage = (name, content, email) => myAxios.post('/addPostComments', { name, content, email })

// 保存博客内容的留言信息
export const addPostCommentsByBlog = (id, name, content, email) => myAxios.post('/addPostCommentsByBlog',{id, name, content, email})

// 保存博客内评论的回复
export const addReplyComment = (id, name, content, email, replyId) => myAxios.post('/addReplyComment',{id, name, content, email, replyId})

// 获取博客详情内的留言
export const getCommentByBlog = id => myAxios.post('/getCommentByBlog',{id})

// 获取博客内容评论的回复信息
export const getReplyComment = id => myAxios.post('/getReplyComment', {id})

//获取指定博客内容
export const getBlogById = id => myAxios.post('/getBlogById', { id })

// 获取下一篇博客
export const getCurrentBlogNext = id => myAxios.post('/getCurrentBlogNext',{id})

// 留言点赞接口:flag一直是1
export const getAddAgree = id => myAxios.post('/addAgree', { flag: FLAGAGREE, id })

// 留言反对接口:flag一直是0
export const getAddOppose = id => myAxios.post('/addOppose', { flag: FLAGOPPOSE, id })