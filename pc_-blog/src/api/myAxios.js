import axios from 'axios'
import { ElMessage } from 'element-plus'
const myAxios = axios.create({
    // baseURL:'http://localhost:8888/api',
    baseURL:'http://110.40.167.170:8889/api',
    // baseURL:'http://yeblog.clubn:8889/api',
    headers: {
        'Cache-control': 'max-age=3600'
    },
    timeout:15000
})
myAxios

myAxios.interceptors.request.use(config =>{
    return config
})

myAxios.interceptors.response.use( 
    response =>{
        return response.data
    },
    error =>{
        //中断
        ElMessage.error('连接服务器失败,请刷新重试')
        return new Promise(()=>{})
    }
)

export default myAxios