<template>
  <el-container class="main">
    <el-aside width="35%" id="aside" >
      <HomeLeft
        :bgColor="RightBackgroundColor"
        @changeBackground="changeRight"
      />
    </el-aside>
    <el-container
      id="right"
      class="right"
      :style="{ backgroundColor: RightBackgroundColor }"
    >
      <!-- <transition name="el-zoom-in-top"> -->
      <transition name="el-fade-in-linear">
        <router-view :bgColor="RightBackgroundColor" class="transition-box" />
      </transition>
    </el-container>
  </el-container>
</template>

<script>
import { ElMessage } from "element-plus";
import { defineComponent, onMounted, onUpdated, ref } from "vue";
import HomeLeft from "../components/HomeLeft";
import { BASEURL, getBgImg } from "@/api/index";
export default defineComponent({
  name: "Home",
  components: { HomeLeft },
  setup() {
    // 左侧背景图切换的间隔时间
    const delayTime = 10000;
    // 左侧背景图容器
    let bgImgArr = ref([]);
    // 左侧背景图总数
    const bgImgSize = ref(0);
    // 默认白天的背景颜色
    let RightBackgroundColor = ref("rgb(250, 251, 255)");
    function changeRight(val) {
      if (val === "morning") {
        RightBackgroundColor.value = "rgb(250, 251, 255)";
      } else {
        RightBackgroundColor.value = "rgb(21, 21, 24)";
      }
    }

    // 获取左侧背景函数
    getBgImgArr();
    async function getBgImgArr() {
      let { code, data, message } = await getBgImg();
      if (code === 200 && message === "成功") {
        bgImgArr.value = data;
        bgImgSize.value = data.length;
        return;
      }
      ElMessage.error("获取左侧背景图失败");
    }

    let elAside;
    let rightId;

    //定时更改背景图
    let img = new Image();
    function changeImg(ele) {
      let index = 0;
      setInterval(() => {
        index >= bgImgSize.value - 1 ? (index = 0) : null;
        img.src = BASEURL + bgImgArr.value[index].imgPath;
        //等待图片加载完成后在切换-->解决白屏问题
        img.onload = function () {
          ele.style.backgroundImage = `url(${BASEURL}${bgImgArr.value[index].imgPath}),url(${BASEURL}/upload/overlay-lba-x2.png)`;
          index++;
        };
      }, delayTime);
    }

    //控制左侧背景图跟随右侧界面滚动而滚动
    function windowAddMouseWheel(rightId, elAside) {
      //图片偏移数
      let count = 0;
      var scrollFunc = function (e) {
        e = e || window.event;
        if (e.wheelDelta) {
          //判断浏览器IE，谷歌滑轮事件
          if (e.wheelDelta > 0) {
            //当滑轮向上滚动时
            if (count == 0) return;
            elAside.style.backgroundPositionY = `${(count += 10)}px`;
          }
          if (e.wheelDelta < 0) {
            //当滑轮向下滚动时
            if (count < -250) return;
            elAside.style.backgroundPositionY = `${(count -= 10)}px`;
          }
        } else if (e.detail) {
          //Firefox滑轮事件
          if (e.detail > 0) {
            //滑轮向下滚动时
            if (count < -250) return;
            elAside.style.backgroundPositionY = `${(count -= 10)}px`;
          }
          if (e.detail < 0) {
            //滑轮向上滚动时
            if (count === 0) return;
            elAside.style.backgroundPositionY = `${(count += 10)}px`;
          }
        }
      };

      //给页面绑定滑轮滚动事件
      if (document.addEventListener) {
        //火狐使用DOMMouseScroll绑定
        rightId.addEventListener("DOMMouseScroll", scrollFunc, false);
      }
      //其他浏览器直接绑定滚动事件
      rightId.onmousewheel = scrollFunc;
    }

    onMounted(() => {
      elAside = document.getElementById("aside");
      rightId = document.getElementById("right");
      changeImg(elAside);
      windowAddMouseWheel(rightId, elAside);
    });

    onUpdated(() => {
      windowAddMouseWheel(rightId, elAside);
    });

    return {
      RightBackgroundColor,
      changeRight,
    };
  },
});
</script>


<style lang="less"  scoped>
.main {
  width: 100%;
  .el-aside {
    position: fixed;
    overflow-y: scroll;
    height: 100%;
    background-image: url("../assets/background1.jpg"),
      url("../assets/overlay-lba-x2.png");
    background-repeat: repeat, no-repeat;
    background-size: cover;
    background-attachment: fixed;
    transition: all 3s;
    -ms-overflow-style: none;
    scrollbar-width: none;
    z-index: 99;
  }
  #right {
    transition: all 3s;
  }
  .el-aside::-webkit-scrollbar {
    display: none;
  }

  .right {
    margin-left: 35%;
    width: 65%;
    min-height: 100vh;
    background: rgba(202, 201, 208, 0.13);
  }
  //淡入淡出
  .transition-box {
    border-radius: 4px;
    box-sizing: border-box;
  }
}
</style>