import { createRouter, createWebHashHistory } from 'vue-router'
const routes = [
  {
    path: '/',
    name: 'Home',
    component: () => import('@/views/Home.vue'),
    children: [
      {
        path: '',
        component: () => import('@/components/HomeRight.vue'),
        meta: {
            title: '主页'
        }
      },
      {
        path: 'articleDetail/:id',
        component: () => import('@/components/ArticleDetail.vue'),
        meta: {
          title: '博客详情页'
        }
      },
      {
        path: 'message',
        component: () => import('@/components/Message.vue'),
        meta: {
          title: '留言页'
        }
      },
    ]
  },
  {
    path: '/:pathMatch(.*)',
    name: '404',
    component: () => import('../views/404')
  },

]

const router = createRouter({
  history: createWebHashHistory(process.env.BASE_URL),
  routes,
})



export default router
