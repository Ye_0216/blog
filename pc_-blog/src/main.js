import { createApp } from 'vue'
import ElementPlus from 'element-plus';
import Bmob from "hydrogen-js-sdk/dist/Bmob-2.2.4.min.js";
import ViLike from "vilike";
import App from './App.vue'
import router from './router'
import 'element-plus/lib/theme-chalk/index.css';
import './assets/icon/iconfont.css'

// 清除项目所有输出语句,生产模式
// console.log = function () {};
createApp(App).use(Bmob).use(ViLike).use(router).use(ElementPlus).mount('#app')
router.beforeEach((to,from,next) =>{
    console.log(to,from,'befor')
    if( to.meta.title ){
        document.title = to.meta.title + '- 叶永洁的博客'
    }
    next()
})

ViLike.configure({
    secretKey: 'b5be54e739b61129', //填写你的 Secret Key ，在 Bmob 后台“设置”-“应用密钥”即可看到 Secret Key；
    safeKey: '520216', //填写你的安全码；
    table: 'visit', //数据表名称；
    key: 'skey', //表字段名称，用于索引；
    visit: 'visit', //表字段名称，用于访问量记录；
    like: 'like' //表字段名称，用于点赞记录；
});
// 初始化
ViLike.init();