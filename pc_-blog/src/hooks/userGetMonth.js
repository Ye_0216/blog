//判断发布的月份距离当前多久
export default function useGetMonth(dateStr){
    let date = new Date()
    let yearOld = dateStr.substring(0,4)
    let monthOld = dateStr.substring(5,7)
    let dayOld = dateStr.substring(8,10)
    let year = date.getFullYear()
    let month = date.getMonth()+1
    let day = date.getDate()
   
    if ( year == yearOld  && month == monthOld){
        return Math.abs(day-dayOld) == 0 ? '今天内' : Math.abs(day-dayOld) +'天'
    }else if( year == yearOld ){
        return yearOld === year ? Math.abs(month-monthOld) + '个月' : (Math.abs(year-yearOld)*12) + Math.abs(month-monthOld)+ '个月'
    }else{
        return yearOld + ' 年 ' + monthOld + '月 ' + dayOld  +'日 '
    }
}