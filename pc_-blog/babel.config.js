module.exports = {
    plugins: [
        "transform-remove-console"
    ]
}

const prodPlugins = []
if (process.env.NODE_ENV === 'production') {
    prodPlugins.push('transform-remove-console')
}
module.exports = {
    plugins: [
        ...prodPlugins
    ]
}