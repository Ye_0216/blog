
var express = require('express');
const cors = require('cors')
const bodyParser = require('body-parser')
// require()
var app = express();

const requestIp = require('request-ip');
app.use(requestIp.mw())

 
//解决跨域
app.use(cors())
app.use(bodyParser.urlencoded({ extended: false }))
// app.use(express.urlencoded({extended:true}))
app.use(express.json()) // 请求体参数是json结构
require('./express')(app)
require('./routes')(app)



app.listen(8888, function () {
  console.log('服务器地址 ：http://localhost:8888');
})
