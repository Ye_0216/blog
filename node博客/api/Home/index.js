// 前端api
let conn = require('../../models/db')
// 分页页数
const pageSize = 5
const Home = {}

// 获取博客主人信息
Home.userInfo = () => {
    return new Promise((res, rej) => {
        conn.query(`select profile,chinesename,englishname,userinfo from user  where userid = 1`, (err, result) => {
            if (err) {
                rej(err.message)
                return
            }
            res(result)
        })
    })
}

/**
 * 根据文章id查询对应文章的具体内容
 */
Home.selectBlogById = (id) => {
    return new Promise((res, rej) => {
        conn.query(`select * from article where id = ${id}`, (err, result) => {
            if (err) {
                rej(err.message)
                return
            }
            res(result)
        })
    })
}

//添加留言
Home.addComment = (name, content, email) => {
    return new Promise((res, rej) => {
        let sql = 'insert into usercomment(name,content,email,agree,oppose,audit) values(?,?,?,?,?,?)'
        let arrSqlParams = [name, content, email, 0, 0, 0]
        conn.query(sql, arrSqlParams, (err, result) => {
            if (err) {
                rej(err.message)
                return
            }
            res(result)
        })
    })
}

// 获取指定id博客的所有留言
Home.onlyRepByBlog = (blogId) => {
    return new Promise((res, rej) => {
        let sql = `SELECT * from usercomment where articleId = ${blogId}`
        conn.query(sql, (err, result) => {
            if (err) {
                rej(err.message)
                return
            }
            res(result)
        })
    })
}

// 为指定博客添加留言
Home.addCommentByBlog = (id, name, content, email) =>{
    return new Promise((res, rej) => {
        let sql = `insert into usercomment(name,content,email,agree,oppose,articleId,audit) values(?,?,?,?,?,?,?)`
        let arrSqlParams = [name, content, email, 0, 0, id, 0]
        conn.query(sql, arrSqlParams, (err, result) => {
            if (err) {
                rej(err.message)
                return
            }
            res(result)
        })
    })
}

// 添加博客内留言评论的回复
Home.addReplyComment = (id, name, content, email, replyId) =>{
    return new Promise((res, rej) => {
        let sql = `insert into usercomment(name,content,email,agree,oppose,articleId,audit,replyId) values(?,?,?,?,?,?,?,?)`
        let arrSqlParams = [name, content, email, 0, 0, id, 0,replyId]
        conn.query(sql, arrSqlParams, (err, result) => {
            if (err) {
                rej(err.message)
                return
            }
            res(result)
        })
    })
}

// 获取博客详情内留言
Home.getCommentByBlog = id =>{
    return new Promise((res, rej) => {
        let sql = `select * from usercomment where articleId = ${id} and audit = 1 and replyId is null`
        conn.query(sql, (err, result) => {
            if (err) {
                rej(err.message)
                return
            }
            res(result)
        })
    })
}

// 获取博客内容评论的回复信息
Home.getReplyComment = id =>{
    return new Promise((res, rej) => {
        let sql = `select * from usercomment where replyId in (select id  from usercomment where replyId = ${id} and audit = 1) union select *  from usercomment where replyId = ${id} and audit = 1 order by date desc`
        conn.query(sql, (err, result) => {
            if (err) {
                rej(err.message)
                return
            }
            res(result)
        })
    })
}

// 查询博客详情页内被回复的评论用户名
Home.getByReplyCommentName = id =>{
    return new Promise((res, rej) => {
        let sql = `select * from usercomment where id = ${id}`
        conn.query(sql, (err, result) => {
            if (err) {
                rej(err.message)
                return
            }
            res(result)
        })
    })
}

// 分页博客
Home.pageBlog = (page) => {
    return new Promise((res, rej) => {
        let sql = `select * from article  order by id desc LIMIT ${(page - 1) * 5}, ${pageSize}`
        conn.query(sql, (err, result) => {
            if (err) {
                rej(err.message)
                return
            }
            res(result)
        })
    })
}
// 获取总的博客数量
Home.getBlogNum = () => {
    return new Promise((res, rej) => {
        let sql = `SELECT count(id)  as length FROM article`
        conn.query(sql, (err, result) => {
            if (err) {
                rej(err.message)
                return
            }
            res(result)
        })
    })
}

// 分页留言
Home.pageRep = (page) => {
    return new Promise((res, rej) => {
        let sql = `select * from usercomment where articleId is null and audit = 1 order by id desc LIMIT ${(page - 1) * 8}, ${8}`
        conn.query(sql, (err, result) => {
            if (err) {
                rej(err.message)
                return
            }
            res(result)
        })
    })
}
// 获取总的留言数量
Home.getRepNum = () => {
    return new Promise((res, rej) => {
        let sql = `SELECT count(id) as length FROM usercomment where articleId is null and audit = 1`
        conn.query(sql, (err, result) => {
            if (err) {
                rej(err.message)
                return
            }
            res(result)
        })
    })
}

// 操作评论表的点赞数
Home.HandleAgree = (id, flag) => {
    return new Promise((res, rej) => {
        let sql
        if (flag === 'add') {
            sql = `update usercomment set agree=agree+1 where id = ${id}`
        } else {
            sql = `update usercomment set agree=agree-1 where id = ${id}`
        }
        conn.query(sql, (err, result) => {
            if (err) {
                rej(err.message)
                return
            }
            res(result)
        })
    })
}
// 操作评论表的反对数
Home.HandleOppose = (id, flag) => {
    return new Promise((res, rej) => {
        let sql
        if (flag === 'add') {
            sql = `update usercomment set oppose=oppose+1 where id = ${id}`
        } else {
            sql = `update usercomment set oppose=oppose-1 where id = ${id}`
        }
        conn.query(sql, (err, result) => {
            if (err) {
                rej(err.message)
                return
            }
            res(result)
        })
    })
}

// 根据当前运行客户端本机的ip，查询ip表中是否存在该记录
Home.isExist = (ipAddress, id) => {
    return new Promise((res, rej) => {
        let sql = `select * from handle_agree_oppose where ipaddress = '${ipAddress}' and commentid = ${id}`
        conn.query(sql, (err, result) => {
            if (err) {
                rej(err.message)
                return
            }
            res(result)
        })
    })
}

// 向操作点赞反对的ip表中添加一条数据
Home.addIpInIpTable = (ipAddress, flag, id) => {
    return new Promise((res, rej) => {
        let sql = `insert into handle_agree_oppose(ipaddress,commentid,flag) values(?,?,?)`
        let arrSqlParams = [ipAddress, id, flag]
        conn.query(sql, arrSqlParams, (err, result) => {
            if (err) {
                rej(err.message)
                return
            }
            res(result)
        })
    })
}

// 修改操作点赞反对的ip表的flag值,1为赞同 0为反对
Home.UpdateIPTableFlag = (flag, id) => {
    return new Promise((res, rej) => {
        let sql = `update handle_agree_oppose set flag = ${flag} where commentid = ${id}`
        conn.query(sql, (err, result) => {
            if (err) {
                rej(err.message)
                return
            }
            res(result)
        })
    })
}

// 获取下一篇博客
Home.getCurrentBlogNext = id =>{
    return new Promise((res, rej) => {
        let sql = `select * from article where id = (select id from article where id < ${id} order by id desc limit 1)`
        conn.query(sql, (err, result) => {
            if (err) {
                rej(err.message)
                return
            }
            res(result)
        })
    })
}

Home.getBlogComment = () => {
    return new Promise((res, rej) => {
        let sql = `select * from usercomment where articleId is not null`
        conn.query(sql, (err, result) => {
            if (err) {
                rej(err.message)
                return
            }
            res(result)
        })
    })
}

module.exports = Home