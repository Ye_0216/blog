let conn = require('../../models/db')

const Admin = {}

//添加新博客
Admin.addBlog = ( title,mainContent,minorContent,img,commentNum)=>{
    return new Promise((res, rej) => {
        let sql = 'insert into article(title,mainContent,minorContent,img,commentNum) values(?,?,?,?,0)'
        let arrSqlParams = [title,mainContent,minorContent,img]
        conn.query(sql,arrSqlParams, (err, result) => {
            if (err) {  
                rej(err.message)
                return
            }
            res(result)
        })
    })
}

//删除指定博客
Admin.delBlog = ( id ) =>{
    return new Promise((res,rej) =>{
        var sql = `delete from article where id = ${id}`
        conn.query(sql,(err,result)=>{
            if (err) {
                rej(err.message)
                return
            }
            res(result)
        })
    })
}



//更新博客
Admin.updateBlog = ( id,title,mainContent,updateTime,minorContent,img ) =>{
    return new Promise((res,rej) =>{
        var sql = `update article set title = '${title}', mainContent = '${mainContent}', updateTime = '${updateTime}', minorContent = '${minorContent}', img = '${img}' where id = ${id}`
        conn.query(sql,(err,result)=>{
            if (err) {
                rej(err.message)
                return
            }
            res(result)
        })

    })
}


//删除指定id留言
Admin.delComments = ( id ) =>{
    return new Promise((res,rej) =>{
        var sql = `delete from usercomment where id = ${id}`
        conn.query(sql,(err,result)=>{
            if (err) {
                rej(err.message)
                return
            }
            res(result)
        })
    })
}



//后台登录
Admin.login = ( name,password ) =>{
    return new Promise((res,rej) =>{
        let sql = `select * from user where username = '${name}' and password = ${password}`
        conn.query(sql,(err,result)=>{
            if (err) {
                rej(err.message)
                return
            }
            res(result)
        })
    })
}
// 更新用户信息
Admin.updateUserInfo = (userid, chinesename, englishname, userinfo, profile) =>{
    return new Promise((res,rej) =>{
        let sql = `update user set chinesename = "${chinesename}", englishname = "${englishname}",userinfo = "${userinfo}",profile = "${profile}" where userid = ${userid}`
        conn.query(sql,(err,result)=>{
            if (err) {
                rej(err.message)
                return
            }
            res(result)
        })
    })
}
//新增背景图
Admin.addBgImg = ( title,imgPath ) =>{
    return new Promise((res,rej) =>{
        let sql = `insert into backgroundimages(title,imgPath) values(?,?)`
        let arrSqlParams = [title,imgPath]
        conn.query(sql,arrSqlParams,(err,result)=>{
            if (err) {
                rej(err.message)
                return
            }
            res(result)
        })
    })
}
//删除背景图
Admin.delBgImg = ( id ) =>{
    return new Promise((res,rej) =>{
        let sql = `delete from backgroundimages where id = ${id}`
        conn.query(sql,(err,result)=>{
            if (err) {
                rej(err.message)
                return
            }
            res(result)
        })
    })
}

// 审核评论
Admin.auditComment = id => {
    return new Promise((res,rej) =>{
        let sql = `UPDATE usercomment set audit = ${1} where id = ${id}`
        conn.query(sql,(err,result)=>{
            if (err) {
                rej(err.message)
                return
            }
            res(result)
        })
    })
}

// 获取访问者信息表
Admin.accessInfo = () =>{
    return new Promise((res,rej) =>{
        let sql = `SELECT * FROM access ORDER BY date DESC`
        conn.query(sql,(err,result)=>{
            if (err) {
                rej(err.message)
                return
            }
            res(result)
        })
    })
}

// 获取数量
Admin.getBlogAndMessgeSize = () => {
    return new Promise((res,rej) =>{
        let sql = `select a.blog,b.comment from
                (select count(*) blog from article) a,
                (select count(*) comment from usercomment) b;`
        conn.query(sql,(err,result)=>{
            if (err) {
                rej(err.message)
                return
            }
            res(result)
        })
    })
}








module.exports = Admin