// 前后台公共api

let conn = require('../models/db')

const Public = {}

// 保存访问者信息
Public.addIp = (ip,Operator,ipAddress) => {
    return new Promise((res,rej) =>{
        var sql = `INSERT into access(ip,Operator,address) VALUES(?,?,?)`
        let sqlArr = [ip,Operator,ipAddress]
        conn.query(sql, sqlArr, (err,result)=>{
            if (err) {
                rej(err.message)
                return
            }
            res(result)
        })
    })
}

// 获取指定id的博客
Public.BlogById = (id) =>{
    return new Promise((res,rej) =>{
        var sql = `select * from article where id = ${id}`
        conn.query(sql,(err,result)=>{
            if (err) {
                rej(err.message)
                return
            }
            res(result)
        })
    })
}

/**
 * 查询所有文章 
 */
 Public.findBlog =  () => {
    return new Promise((res, rej) => {
        conn.query('select * from article order by id desc', (err, result) => {
            if (err) {
                rej(err.message)
                return
            }
            res(result)
        })
    })
}
//获取所有留言内容
Public.findComment = () => {
    return new Promise((res,rej) =>{
        let sql = 'select * from usercomment order by id desc'
        conn.query(sql, (err, result) => {
            if (err) {
                rej(err.message)
                return
            }
            res(result)
        })
    })
}
// 获取所有动画背景图 
Public.getBgImg = () => {
    return new Promise((res,rej) =>{
        let sql = 'select * from backgroundimages order by id desc'
        conn.query(sql, (err, result) => {
            if (err) {
                rej(err.message)
                return
            }
            res(result)
        })
    })
}


module.exports = Public