/* *************后台控制层************* */
// var conn = require('../models/db')
let multiparty = require('multiparty')
let fs = require('fs')
let Admin = require('../api/Admin/index')
let code = { code: 200, message: '成功', data: null }
let err_code = { code: 201, message: '账号或密码错误' }


//添加新博客
exports.addBlog = async (req, res) => {
    let { title, mainContent, minorContent, img } = req.body
    try {
        let result = await Admin.addBlog(title, mainContent, minorContent, img)
        code.data = result.insertId
        res.send(code)
    } catch (error) {
        catchFn(error, res)
    }
}

//删除指定博客
exports.delBlog = async (req, res) => {
    try {
        let { id } = req.body    
        let result = await Admin.delBlog(id)
        code.data = result.insertId
        res.send(code)
    } catch (error) {
        catchFn(error, res)
    }
}

// // 获取指定博客
// exports.BlogById = async (req, res) => {
//     let {id} = req.body
//     let result = await Admin.BlogById(id)
//     if (result) {
//         code.data = result.insertId
//         res.send(code)
//         return
//     }
//     res.send(err_code)
// }
//更新指定博客
exports.updateBlog = async (req, res) => {
    try {
        let { id, title, mainContent, updateTime, minorContent, img } = req.body
        let result = await Admin.updateBlog(id, title, mainContent, updateTime, minorContent, img)
        code.data = result.insertId
        res.send(code)
    } catch (error) {
        catchFn(error, res)
    }
}

//删除指定留言
exports.delComments = async (req, res) => {
    try {
        let { id } = req.body
        let result = await Admin.delComments(id)
        code.data = result.insertId
        res.send(code)
    } catch (error) {
        catchFn(error, res)
    }

}

// 登录接口
exports.login = async (req, res) => {
    try {
        let { name, password } = req.body
        let result = await Admin.login(name, password)
            code.data = result[0]
            res.send(code)
    } catch (error) {
        catchFn(error, res)
    }
}

// 更新用户信息
exports.updateUserInfo = async (req, res) =>{
    try {
        let { userid, chinesename, englishname, userinfo, profile} = req.body
        let result = await Admin.updateUserInfo( userid, chinesename, englishname, userinfo, profile)
        code.data = result
        res.send(code)
    } catch (error) {
        catchFn(error, res)
    }
}
// elementUi的upload组件必须的action对应接口
exports.uploadAction = async (req, res) => {
    res.send('action成功')
}
// 图片上传保存接口
exports.upload = async (req, res) => {
    let uploadDir = './' //这个不用改，因为并不是保存在这个目录下，这只是作为中间目录，待会要重命名文件到指定目录的
    //利用multiparty中间件获取文件数据
    let form = new multiparty.Form()
    form.uploadDir = uploadDir
    form.keepExtensions = true; //是否保留后缀
    form.parse(req, function (err, fields, files) { //其中fields表示你提交的表单数据对象，files表示你提交的文件对象
        //这里是save_path 就是前端传回来的 path 字段，这个字段会被 multiparty 中间件解析到 fields 里面 ，这里的 fields 相当于 req.body 的意思
		let save_path = fields.path 
        
        if (err) {
            err_code.message = err.message
            res.send(err_code)
        } else {
            files.file.forEach(file => {
                /*
                 * file.path 文件路径
                 * save_path+originalFilename   指定上传的路径 + 原来的名字
                 */
                fs.rename(file.path, './public/upload/'+ file.originalFilename, function (err) {
                    if(err) {
                        err_code.message = err.message
                        res.send(err_code)
                    }else{
                        code.data = `/upload/${file.originalFilename}`
                        res.send(code)
                    }
                }); 
            })
        }
    })
}

// 新增背景图
exports.addBgImg = async (req,res ) =>{
    try {
        
    } catch (error) {
        catchFn(error, res)
    }

    let { title,imgPath } = req.body
    let result = await Admin.addBgImg(title,imgPath)
    if (result) {
        code.data = result.insertId
        res.send(code)
        return
    }
    res.send(err_code)
}
// 删除背景图
exports.delBgImg = async (req,res) =>{
    try {
        let { id } = req.body
        await Admin.delBgImg(id)
        res.send(code)
    } catch (error) {
        catchFn(error, res)
    }
}
// 审核留言
exports.auditComment = async (req, res) =>{
    try {
        let { id } = req.body
        await Admin.auditComment(id)
        res.send(code)
    } catch (error) {
        catchFn(error, res)
    }
}

// 获取访问信息
exports.accessInfo = async (req, res) =>{
    try {
        let result = await Admin.accessInfo()
        code.data = result
        res.send(code)
    } catch (error) {
        catchFn(error, res)
    }
}

// 获取总数量
exports.getBlogAndMessgeSize = async (req, res) =>{
    try {
        let result = await Admin.getBlogAndMessgeSize()
        code.data = result[0]
        res.send(code)
    } catch (error) {
        catchFn(error, res)
    }
}

// 异常响应函数
function catchFn(err, res){
    err_code.message = err
    res.send(err_code)
}