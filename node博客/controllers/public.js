/* *************后台公共接口控制层************* */


let Public = require('../api/index')
let code = { code: 200, message: '成功', data: null }
let err_code = { code: 201, message: '失败' }


// 获取指定博客
exports.BlogById = async (req, res) => {
    try {
        let { id } = req.body
        let result = await Public.BlogById(id)
        code.data = result[0]
        res.send(code)
    } catch (error) {
        catchFn(error, res)
    }
}


//获取所有博客
exports.getArticle = async (req, res) => {
    try {
        let result = await Public.findBlog()
        // 去除文章内容的值
        result = result.map(item => {
            delete item['mainContent']
            return item
        })
        code.data = result
        res.send(code)
    } catch (error) {
        catchFn(error, res)
    }
}

//获取留言板内容
exports.getRep = async function (req, res) {
    try {
        const result = await Public.findComment()
        code.data = result
        res.send(code)
    } catch (error) {
        catchFn(error, res)
    }
}
// 获取所有动画切换背景图
exports.getBgImg = async (req, res) => {
    try {
        const result = await Public.getBgImg()
        code.data = result
        res.send(code)
    } catch (error) {
        catchFn(error, res)
    }
}

// 异常响应函数
function catchFn(err, res){
    err_code.message = err
    res.send(err_code)
}