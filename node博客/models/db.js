var mysql = require('mysql');
// 部署的
// var conn = mysql.createConnection({
//   host     : '110.40.167.170',
//   pror     : 3306,  
//   user     : 'blog',
//   password : '123456',
//   database : 'blog'
// });
// 开发的
var conn = mysql.createConnection({
  host     : 'localhost',
  user     : 'root',
  password : '123456',
  database : 'blog'
});
// 连接
conn.connect(function (err,dos) {
  if (err) {
      console.log('连接数据库失败');
      return
  }
  console.log('连接数据库成功');
});


module.exports = conn