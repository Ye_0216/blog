let home = require('./controllers/home')
let admin = require('./controllers/admin')

let public = require('./controllers/public')

/**
 * 路由
 */
module.exports = function (app) {
  // 公共api
  //获取所有博客
  app.get('/api/getArticle', public.getArticle);
  //获取所有留言
  app.get('/api/getRep', public.getRep)
  // 获取所有背景图
  app.get('/api/getBgImg', public.getBgImg)
  // 获取唯一的博客内容
  app.post('/api/getBlogById', public.BlogById)



  //前台api
  // 获取博客用户信息
  app.get('/api/userInfo', home.userInfo)
  //获取指定id的博客
  app.post('/api/getArticleById', home.getArticleById)
  // 获取指定id博客的所有留言
  app.post('/api/onlyRep', home.onlyRep)
  //保存留言
  app.post('/api/addPostComments', home.addPostComments)

  // 根据分页获取博客列表，每页5条
  app.post('/api/pageBlog', home.pageBlog)
  // 根据分页获取留言列表，每页5条
  app.post('/api/pageRep', home.pageRep)
  // 获取当前浏览博客的下一条博客内容,携带参数:id:当前浏览博客的id
  app.post('/api/getCurrentBlogNext', home.getCurrentBlogNext)
  // 评论点赞接口,携带参数flag:1和该点赞的评论的唯一id
  app.post('/api/addAgree', home.addAgree)
  // 评论反对接口,携带参数flag:0和该点赞的评论的唯一id
  app.post('/api/addOppose', home.addOppose)
  // 保存博客详情页内的留言
  app.post('/api/addPostCommentsByBlog',home.addPostCommentsByBlog)
  // 保存博客详情页内的留言回复
  app.post('/api/addReplyComment',home.addReplyComment)
  // 获取博客详情页内的留言
  app.post('/api/getCommentByBlog',home.getCommentByBlog)
  // 获取博客内容评论的回复信息
  app.post('/api/getReplyComment',home.getReplyComment)


  //后台api
  //添加新博客
  app.post('/api/addBlog', admin.addBlog)
  // 删除博客
  app.post('/api/delBlog', admin.delBlog)
  //更新博客
  app.post('/api/updateBlog', admin.updateBlog)
  //删除指定留言内容
  app.post('/api/delComments', admin.delComments)
  //登录接口
  app.post('/api/login', admin.login)
  // 图片上传接口
  app.post('/api/upload', admin.upload)
  app.post('/api/uploadAction', admin.uploadAction)
  // 新增背景图
  app.post('/api/addBgImg', admin.addBgImg)
  // 删除背景图
  app.post('/api/delBgImg', admin.delBgImg)
  // 更新当前用户信息
  app.post('/api/updateUserInfo', admin.updateUserInfo)
  // 审核留言,让其能够在前端展示
  app.post('/api/auditComment',admin.auditComment)
  // 获取访问者信息表的信息
  app.post('/api/access',admin.accessInfo)
  // 获取总数量(博客，留言)
  app.get('/api/getBlogAndMessgeSize',admin.getBlogAndMessgeSize)
}
