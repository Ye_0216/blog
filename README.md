# Blog

#### 介绍
前后端分离的个人博客，基于node后端和vue前端技术,前端界面使用Vue3技术,后台管理界面使用Vue2

#### 技术点
Vue3/2全家桶,Axios，ElementUI，Node.js,Less


#### 安装教程

1.  克隆该项目
2.  导入sql文件
3.  进入node博客目录运行: yarn ,等结束后元运行y node app.js命令
4.  进入pc_blog目录运行:  yarn ,等结束后元运行yarn serve 
5.  进入fantastic-admin目录运行:  yarn ,等结束后元运行yarn serve 


#### 最后
1. 麻烦点个Star



